(function ($) {
    "use strict";

    // Spinner
    var spinner = function () {
        setTimeout(function () {
            if ($('#spinner').length > 0) {
                $('#spinner').removeClass('show');
            }
        }, 1);
    };
    spinner();


    // Initiate the wowjs
    new WOW().init();


    // Sticky Navbar
    $(window).scroll(function () {
        if ($(this).scrollTop() > 45) {
            $('.navbar').addClass('sticky-top shadow-sm');
        } else {
            $('.navbar').removeClass('sticky-top shadow-sm');
        }
    });


    // Dropdown on mouse hover
    const $dropdown = $(".dropdown");
    const $dropdownToggle = $(".dropdown-toggle");
    const $dropdownMenu = $(".dropdown-menu");
    const showClass = "show";

    $(window).on("load resize", function () {
        if (this.matchMedia("(min-width: 992px)").matches) {
            $dropdown.hover(
                function () {
                    const $this = $(this);
                    $this.addClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "true");
                    $this.find($dropdownMenu).addClass(showClass);
                },
                function () {
                    const $this = $(this);
                    $this.removeClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "false");
                    $this.find($dropdownMenu).removeClass(showClass);
                }
            );
        } else {
            $dropdown.off("mouseenter mouseleave");
        }
    });


    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 1500, 'easeInOutExpo');
        return false;
    });


    // Testimonials carousel
    $(".testimonial-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        margin: 25,
        dots: false,
        loop: true,
        center: true,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });


    // Portfolio isotope and filter
    var portfolioIsotope = $('.portfolio-container').isotope({
        itemSelector: '.portfolio-item',
        layoutMode: 'fitRows'
    });
    $('#portfolio-flters li').on('click', function () {
        $("#portfolio-flters li").removeClass('active');
        $(this).addClass('active');

        portfolioIsotope.isotope({ filter: $(this).data('filter') });
    });

})(jQuery);

$('#exampleModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('New message to ' + recipient)
    modal.find('.modal-body input').val(recipient)
})


// Makna Logo
const containerMakna = document.querySelector('#container-makna');
const maknaLogo = document.querySelector('.img-makna');
const bg = document.getElementById('img-bg');
const logo = document.querySelector('.img-2022');
const definisiLogo = document.querySelector('#definisi-logo');
const definisiWarna = document.querySelector('#definisi-warna');
const defMakna = document.querySelector('#logo-makna');

definisiLogo.style.display = 'none';
definisiWarna.style.display = 'none';

maknaLogo.addEventListener('click', () => {
    if (definisiLogo.style.display === 'none' && definisiWarna.style.display === 'none') {
        definisiLogo.style.display = 'block';
        defMakna.style.display = 'none';
        bg.style.opacity = 0.5;
        logo.style.opacity = 0.5;
        maknaLogo.style.opacity = 1;

    }
    else if (definisiLogo.style.display === 'none' && definisiWarna.style.display === 'block') {
        definisiWarna.style.display === 'none';
        definisiLogo.style.display === 'block';
        bg.style.opacity = 0.5;
        maknaLogo.style.opacity = 1;
        logo.style.opacity = 0.5;
    }
    else {
        definisiLogo.style.display = 'none';
        bg.style.opacity = 1;
        logo.style.opacity = 1;
        defMakna.style.display = 'block';
    }

})

logo.addEventListener('click', () => {
    if (definisiWarna.style.display === 'none' && definisiLogo.style.display === 'none') {
        definisiWarna.style.display = 'block';
        defMakna.style.display = 'none';
        bg.style.opacity = 0.5;
        maknaLogo.style.opacity = 0.5;
        definisiLogo.style.display = 'none';
        logo.style.opacity = 1;
    }
    else if (definisiWarna.style.display === 'none' && definisiLogo.style.display === 'block') {
        definisiLogo.style.display === 'none';
        definisiWarna.style.display === 'block';
        bg.style.opacity = 0.5;
        maknaLogo.style.opacity = 0.5;
        logo.style.opacity = 1;
    }
    else {
        definisiWarna.style.display = 'none';
        bg.style.opacity = 1;
        maknaLogo.style.opacity = 1;
        defMakna.style.display = 'block';
    }

})

// Search Function 
// const searchForm = document.getElementById('searchForm');
// const searchResults = document.getElementById('searchResults');

// searchForm.addEventListener('submit', (event) => {
//     event.preventDefault();
//     const searchQuery = document.getElementById('searchQuery').value;

//     // Send the search query to the server using AJAX
//     fetch('search.php', {
//         method: 'POST',
//         headers: {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         },
//         body: 'searchQuery=' + encodeURIComponent(searchQuery)
//     })
//     .then(response => response.text())
//     .then(data => {
//         // Display the search results
//         searchResults.innerHTML = data;
//     })
//     .catch(error => {
//         console.error('Error fetching search results:', error);
//     });
// });

const searchForm = document.getElementById('searchForm');

searchForm.addEventListener('submit', (event) => {
    event.preventDefault();
    const searchQuery = document.getElementById('searchQuery').value;

    // Redirect the user to search_result.php with the search query as a URL parameter
    window.location.href = 'search_result.php?searchQuery=' + encodeURIComponent(searchQuery);
});

