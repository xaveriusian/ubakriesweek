<!-- search_result.php -->
<!DOCTYPE html>
<html>
<head>
    <title>Student Profile</title>
    <!-- Add Bootstrap CSS link -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles.css">
    <style>
        .navbar-light .navbar-nav .nav-link {
            color :#fff!important;
        }
        </style>
</head>
<body>
    <div class="container-xxl position-relative p-0">
    <nav class="navbar navbar-expand-lg navbar-light" style="margin-bottom:50px;padding:30px;background-color:#85171a;color:#fff;">

                <a href="" class="navbar-brand p-0">
                    <!-- <h1 class="m-0"><i class="fa fa-search me-2"></i>SEO<span class="fs-5">Master</span></h1> -->
                    <img src="img/logo.png" alt="Logo" class="logo-navbar" style="width:50%;">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0" style="color:$fff;">
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Home</a>
                            <div class="dropdown-menu m-0">
                                <a href="#mengenal" class="dropdown-item">Mengenal UBakrie's Week 2022</a>
                                <a href="#maknalogo" class="dropdown-item">Makna Logo UBakrie's Week 2022</a>
                                <a href="#makna" class="dropdown-item">Makna Tema</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">About Us</a>
                            <div class="dropdown-menu m-0">
                                <a href="peta.html" class="dropdown-item">Peta Perjalanan UBakrie's Week</a>
                                <a href="divisi.html" class="dropdown-item">Divisi Kepanitiaan</a>
                                <a href="peta.html#value" class="dropdown-item">Value UBakrie & UBakrie's Week 2022</a>
                                <a href="peta.html#faq" class="dropdown-item">FAQs</a>
                                <a href="https://bima.bakrie.ac.id/index.php?option=com_content&view=article&id=118&Itemid=155" class="dropdown-item">Layanan Kemahasiswaan</a>
                            </div>
                        </div>
                        <a href="https://news.bakrie.ac.id/ubakriesweek2022" class="nav-item nav-link">News</a>
                        <a href="#footer" class="nav-item nav-link">Pranala</a>
                    </div>
                    <div style="margin-top: 30px;margin-left: 30px;">
                        <!-- <h1>Search Function</h1> -->
                        <form id="searchForm" method="post" action="search_result.php">
                            <input type="text" name="searchQuery" id="searchQuery" placeholder="Search...">
                            <button type="submit" style="border-radius:10px;width:45%;float:right;">Search</button>
                        </form>
                    </div>
                    <!-- <butaton type="button" class="btn text-secondary ms-3" data-bs-toggle="modal" data-bs-target="#searchModal"><i class="fa fa-search"></i></butaton>
                    <a href="https://htmlcodex.com/startup-company-website-template" class="btn btn-secondary text-light rounded-pill py-2 px-4 ms-3">Pro Version</a> -->
                </div>
            </nav>
        </div>
    <div class="container">
        <h1>Student Profile</h1>
        <br>
        <div id="searchResults">
            <?php
            // Include the database connection code here
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "mahasiswa";

            // Create a connection to the database
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            // Fetch the student's profile information
            if (isset($_GET['searchQuery'])) {
                $searchQuery = $_GET['searchQuery'];
                $searchTerm = "%" . $searchQuery . "%";

                // Prepare and execute the SQL query to search for students
                $stmt = $conn->prepare("SELECT * FROM student WHERE name LIKE ? OR email LIKE ?");
                $stmt->bind_param("ss", $searchTerm, $searchTerm);
                $stmt->execute();

                // Check if the query was successful and returned a valid result
                $result = $stmt->get_result();
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo '<div class="card">';
                        echo '<div class="card-body">';
                        echo '<h5 class="card-title">' . $row['name'] . '</h5>';
                        echo '<p class="card-text">Email: ' . $row['email'] . '</p>';
                        // Add more profile details as needed
                        echo '</div>';
                        echo '</div>';
                    }
                } else {
                    echo "<p>No results found.</p>";
                }

                $stmt->close();
            }

            // Close the database connection
            $conn->close();
            ?>
        </div>
       <button style="border-radius:10px;background-color:#85171a;color:#fff;margin-top:20px;margin-bottom:20px;"> <a href="index.html" style="color:#fff;">Back to Search</a> </button>
    </div>

    <!-- Add Bootstrap JS and jQuery scripts (required for Bootstrap features) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
