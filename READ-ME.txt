  =>  Template Name    : UBARKIE'S WEEK 2022

  =>  Template Link    : https://bakrie.ac.id/ubakriesweek2022

  =>  Template License : Biro Teknik Informatika UBakrie

  =>  Template Author  : Jacob Septianus Wijaya

  =>  Author Website   : https://jacobwijaya.site

  Structure File

---- css
----- bootstrap.min.css
----- style.css
---- img
----- all image asset
---- js
----- main.js
------- index.html
------- peta.html
------- divisi.html
------- 404.html
------- valuekampus.html
------- valueapi.html

=========== NATIVE WEB PAGE =========== 
