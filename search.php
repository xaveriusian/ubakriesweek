<?php
// Database connection details
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mahasiswa";

// Create a connection to the database
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Check if search query is set
if (isset($_POST['searchQuery'])) {
    $searchQuery = $_POST['searchQuery'];

    // Prepare and execute the SQL query to search for students
    $stmt = $conn->prepare("SELECT * FROM student WHERE name LIKE ? OR email LIKE ?");
    $searchTerm = "%" . $searchQuery . "%";
    $stmt->bind_param("ss", $searchTerm, $searchTerm); 
    $stmt->execute();
    $result = $stmt->get_result();

    // Display search results
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "<p>Name: " . $row['name'] . "</p>";
            echo "<p>Email: " . $row['email'] . "</p>";
            echo "<hr>";
        }
    } else {
        echo "<p>No results found.</p>";
    }

    $stmt->close();
}

// Close the database connection
$conn->close();
?>
